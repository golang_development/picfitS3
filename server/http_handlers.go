package server

import (
	"context"
	"fmt"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/mholt/binding"
	"github.com/pkg/errors"

	api "gopkg.in/fukata/golang-stats-api-handler.v1"

	"github.com/thoas/picfit"
	"github.com/thoas/picfit/constants"
	"github.com/thoas/picfit/failure"
	"github.com/thoas/picfit/payload"
)

type handlers struct {
	processor *picfit.Processor
}

func (h handlers) stats(c *gin.Context) {
	c.JSON(http.StatusOK, api.GetStats())
}

func (h handlers) panic(c *gin.Context) {
	panic(errors.WithStack(fmt.Errorf("KO")))
}

func (h handlers) error(c *gin.Context) {
	c.JSON(http.StatusInternalServerError, struct {
		Message string    `json:"message"`
		Time    time.Time `json:"time"`
	}{
		Message: "Internal server error",
		Time:    time.Now(),
	})
}

// healthcheck displays an ok response for healthcheck
func (h handlers) swagger(startedAt time.Time) func(c *gin.Context) {
	return func(c *gin.Context) {
		now := time.Now().UTC()

		uptime := now.Sub(startedAt)

		c.JSON(http.StatusOK, gin.H{
			"started_at":            startedAt.String(),
			"uptime":                uptime.String(),
			"status":                "Ok",
			"version":               constants.Version,
			"revision":              constants.Revision,
			"build_time":            constants.BuildTime,
			"compiler":              constants.Compiler,
			"latest_commit_message": constants.LatestCommitMessage,
			"ip_address":            c.ClientIP(),
		})
	}
}

// Register godoc
// @Summary healthcheck displays an ok response for healthcheck
// @Schemes
// @Description healthcheck displays an ok response for healthcheck
// @Tags DEFAULT
// @Produce json
// @Router /healthcheck [get]
func (h handlers) healthcheck(startedAt time.Time) func(c *gin.Context) {
	return func(c *gin.Context) {
		now := time.Now().UTC()

		uptime := now.Sub(startedAt)

		c.JSON(http.StatusOK, gin.H{
			"started_at":            startedAt.String(),
			"uptime":                uptime.String(),
			"status":                "Ok",
			"version":               constants.Version,
			"revision":              constants.Revision,
			"build_time":            constants.BuildTime,
			"compiler":              constants.Compiler,
			"latest_commit_message": constants.LatestCommitMessage,
			"ip_address":            c.ClientIP(),
		})
	}
}

// Register godoc
// @Summary Display the image, useful when you are using an img tag.
// @Schemes
// @description.markdown  display
// @Tags CONVERT
// @Param   path    query    string  true  "The filepath to load the image using your source storage" example(1.jpg 2.jpg ... 5.jpg) default(2.jpg)
// @Param   w    query    integer  true  "The desired width of the image, if 0 is provided the service will calculate the ratio with height" example(300,400,0) default(0)
// @Param   h    query    integer  true  "The desired height of the image, if 0 is provided the service will calculate the ratio with width" example(0,400,550) default(300)
// @Param   op    query    string  true  "The operation to perform" example(resize thumbnail flip rotate flat) default(resize)
// @Router /display [get]
func (h handlers) display(c *gin.Context) error {
	file, err := h.processor.ProcessContext(c,
		picfit.WithLoad(true))
	if err != nil {
		return err
	}

	for k, v := range file.Headers {
		c.Header(k, v)
	}

	c.Header("Cache-Control", "must-revalidate")

	c.Data(http.StatusOK, file.ContentType(), file.Content())

	return nil
}

// upload uploads an image to the destination storage

// Register godoc
// @Summary upload uploads an image to the destination storage
// @Schemes
// @description.markdown upload
// @Tags UTILS
// @Success		200		{string}	string	"ok" 
// @Router /upload [post]
func (h handlers) upload(c *gin.Context) error {
	multipartPayload := new(payload.Multipart)
	if err := binding.Bind(c.Request, multipartPayload); err != nil {
		return err
	}

	file, err := h.processor.Upload(context.Background(), multipartPayload)
	if err != nil {
		return err
	}

	c.JSON(http.StatusOK, gin.H{
		"filename": file.Filename(),
		"path":     file.Path(),
		"url":      file.URL(),
	})

	return nil
}

// Register godoc
// @Summary delete deletes a file from storages
// @Schemes
// @description.markdown delete
// @Tags UTILS
// @Router / [delete]
func (h handlers) delete(c *gin.Context) error {
	var (
		err         error
		path        = c.Param("parameters")
		key, exists = c.Get("key")
		ctx         = c.Request.Context()
	)

	if path == "" && !exists {
		return failure.ErrUnprocessable
	}

	if !exists {
		err = h.processor.Delete(ctx, path[1:])
	} else {
		err = h.processor.DeleteChild(ctx, key.(string))
	}

	if err != nil {
		return err
	}

	c.String(http.StatusOK, "Ok")

	return nil
}

// get generates an image synchronously and return its information from storages

// Register godoc
// @Summary Retrieve information about an image.
// @Schemes
// @description.markdown  get
// @Tags CONVERT
// @Param   path    query    string  true  "The filepath to load the image using your source storage" example(1.jpg 2.jpg ... 5.jpg) default(2.jpg)
// @Param   w    query    integer  true  "The desired width of the image, if 0 is provided the service will calculate the ratio with height" example(300) default(0)
// @Param   h    query    integer  true  "The desired height of the image, if 0 is provided the service will calculate the ratio with width" example(400) default(400)
// @Param   op    query    string  true  "The operation to perform" example(Resize Thumbnail Flip Rotate Flat) default(resize)
// @Router /get [get]
func (h handlers) get(c *gin.Context) error {
	file, err := h.processor.ProcessContext(c,
		picfit.WithLoad(false))
	if err != nil {
		return err
	}

	c.JSON(http.StatusOK, gin.H{
		"filename": file.Filename(),
		"path":     file.Path(),
		"url":      file.URL(),
		"key":      file.Key,
	})

	return nil
}

// redirect redirects to the image using base url from storage
func (h handlers) redirect(c *gin.Context) error {
	file, err := h.processor.ProcessContext(c,
		picfit.WithLoad(false))
	if err != nil {
		return err
	}

	c.Redirect(http.StatusMovedPermanently, file.URL())

	return nil
}

func pprofHandler(h http.HandlerFunc) gin.HandlerFunc {
	return func(c *gin.Context) {
		h.ServeHTTP(c.Writer, c.Request)
	}
}
