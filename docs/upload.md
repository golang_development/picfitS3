Upload
------

Upload is disabled by default for security reason, you can enable
it in your config:

``config.json``

.. code-block:: json

    {
      "options": {
        "enable_upload": true
      }
    }

To work properly, the input field must be named "data"

Example:

``curl -F data=@data/6.jpg https://agami.vedomosti.cloud/upload``