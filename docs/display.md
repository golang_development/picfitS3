### Display the image
useful when you are using an **img tag**.  
The generated image will be stored asynchronously on your destination storage backend.