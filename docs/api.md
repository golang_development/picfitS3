# General API documentation

Reusable Go server to manipulate images (resize, thumbnail, etc.).

it will act as a proxy on your storage engine and will be served ideally behind an HTTP cache system like **memcache,varnish,cloudflare**.  
It supports multiple storage backends and multiple key/value stores.

The following backends are supported:

* Amazon S3
* Google Cloud Storage
* File system
* Minio Object Store

### Examples:
`https://agami.vedomosti.cloud/display/resize/0x500/5.jpg`
`https://agami.vedomosti.cloud/display/resize/300x0/1.jpg`

## Result on:

[https://minio.demoproject.one/](https://minio.demoproject.one/)

`login: demoproject`
`password: demoproject`
