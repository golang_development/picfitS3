### Retrieve information about an image.
our file will be generated synchronously then you will get the following information:

* **filename** - Filename of your generated file
* **path** - Path of your generated file
* **url** - Absolute url of your generated file (only if ``base_url`` is available on your destination storage)

The first query will be **slower but next ones will be faster** because the name
of the generated file will be stored in your key/value store.

Expect the following result:

```json

    {
        "filename":"a661f8d197a42d21d0190d33e629e4.png",
        "path":"cache/6/7/a661f8d197a42d21d0190d33e629e4.png",
        "url":"https://ds9xhxfkunhky.cloudfront.net/cache/6/7/a661f8d197a42d21d0190d33e629e4.png"
    }
```