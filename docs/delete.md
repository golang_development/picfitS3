Deletion
--------

Deletion is disabled by default for security reason, you can enable
it in your config:

``config.json``

.. code-block:: json

    {
      "options": {
        "enable_delete": true
      }
    }

You will be able to delete root image and its children, for example if you upload an image with
the file path `/6.jpg`, you can delete the main image on stockage by sending the following HTTP request:


::

   DELETE https://agami.vedomosti.cloud/6.jpg

or delete a child:

::

   DELETE https://agami.vedomosti.cloud/display/thumbnail/100x100/6.jpg

If you want to delete the main image and cascade its children, you can enable it in your config:

``config.json``

.. code-block:: json

    {
      "options": {
        "enable_delete": true,
        "enable_cascade_delete": true
      }
    }

when a new image will be processed, it will be linked to the main image and stored in the kvstore.